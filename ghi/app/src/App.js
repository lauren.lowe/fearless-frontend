import React from 'react';
import Nav from './Nav';
import './App.css';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import { BrowserRouter,
        Routes,
        Route } from "react-router-dom";
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path = "/" element={<App />} />
            <Route index element={<MainPage />} />
          <Route path= "locations">
            <Route path="new" element ={<LocationForm />} />
          </Route>
          <Route path = "conferences">
            <Route path= "new" element ={<ConferenceForm/>} />
          </Route>
          {/* <Route path ="attendees">
          <Route  path = "new" element ={<AttendConferenceForm/>} />
            <Route path = "props.attendees" element ={<AttendeesList/>} />
          </Route> */}
          <Route path ="attendees/new" element={<AttendConferenceForm/>} />
          <Route path="attendees" element = {<AttendeesList attendees={props.attendees} />} />
          <Route path ="presentations/new" element={<PresentationForm/>} />
        </Routes>
        {/* <AttendeesList attendees={props.attendees} /> */}
      </div>
    </BrowserRouter>
  );
}

export default App;
